# Dockerfile springboot app
1. Make a dockerfile for this simple springboot app. (tip: you need to build the maven project before building the dockerfile)
2. Upgrade your dockerfile to a multistage dockerfile, so you do not need to build the maven project anymore. This should be done with building the dockerfile.
